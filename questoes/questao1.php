<?php
/*
*   1. Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima
*      “Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos
*       de ambos (3 e 5), imprima “FizzBuzz”.
*/
?>
<!doctype html>

<html lang="pt-BR">
<head>
    <meta charset="utf-8">

    <title>Prova BDR - Questão 1</title>
    <meta name="description" content="Prova-BDR">
    <meta name="author" content="Thiago Natanael">
</head>

<body>
	<a href=javascript:history.back(2)>Voltar</a>
	<br />
	<h3>Resposta:</h3>
    <code>
    &nbsp;&nbsp;for($i=1;$i<=100;$i++) {<br />
    &nbsp;&nbsp;&nbsp;&nbsp;echo (($i % 3) ? $i : 'Fizz'.(($i % 5) ? null: 'Buzz')).'< br/ >';<br />
    &nbsp;&nbsp;}<br />
    </code>
    <h3>Resultado:</h3>
	<?php
        for($i=1;$i<=100;$i++) {
    		echo (($i % 3) ? $i : 'Fizz'.(($i % 5) ? null: 'Buzz')).'<br/>';
		}
    ?>
</body>
</html>