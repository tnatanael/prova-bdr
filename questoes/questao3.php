<?php
/*
*	3. Refatore o código abaixo, fazendo as alterações que julgar necessário.
*/
?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">

    <title>Prova BDR - Questão 3</title>
    <meta name="description" content="Prova-BDR">
    <meta name="author" content="Thiago Natanael">
</head>
<body>
	<a href=javascript:history.back(2)>Voltar</a>
	<br />
	<h3>Código Original:</h3>
	<code>
	class MyUserClass<br />
	{<br />
	&nbsp;&nbsp;public function getUserList()<br />
	&nbsp;&nbsp;{<br />
	&nbsp;&nbsp;&nbsp;&nbsp;$dbconn = new DatabaseConnection('localhost','user','password');<br />
	&nbsp;&nbsp;&nbsp;&nbsp;$results = $dbconn->query('select name from user');<br />
	&nbsp;&nbsp;&nbsp;&nbsp;sort($results);<br />
	&nbsp;&nbsp;&nbsp;&nbsp;return $results;<br />
	&nbsp;&nbsp;}<br />
	}<br />
	</code>
	<br />
	<br />
	<h3>Código Refatorado:</h3>
	<code>
	class MyUserClass<br />
	{<br />
	&nbsp;&nbsp;protected $dbconn;<br />
	<br />
	&nbsp;&nbsp;function __construct(){<br />
	&nbsp;&nbsp;&nbsp;&nbsp;$this->dbconn = new DatabaseConnection('localhost','user','password');<br />
	&nbsp;&nbsp;}<br />
	<br />
	&nbsp;&nbsp;public function getUserList()<br />
	&nbsp;&nbsp;{<br />
	&nbsp;&nbsp;&nbsp;&nbsp;return $this->dbconn->query('SELECT name FROM user ORDER BY name');<br />
	&nbsp;&nbsp;}<br />
	}<br />
	</code>
</body>
</html>