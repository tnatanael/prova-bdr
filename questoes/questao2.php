<?php
/*
*	2. Refatore o código abaixo, fazendo as alterações que julgar necessário.
*/
?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">

    <title>Prova BDR - Questão 2</title>
    <meta name="description" content="Prova-BDR">
    <meta name="author" content="Thiago Natanael">
</head>
<body>
		<a href=javascript:history.back(2)>Voltar</a>
	<br />
	<h3>Código Original:</h3>
	<code>
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {<br />
	&nbsp;&nbsp;header("Location: http://www.google.com");<br />
	&nbsp;&nbsp;exit();<br />
	} elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {<br />
	&nbsp;&nbsp;header("Location: http://www.google.com");<br />
	&nbsp;&nbsp;exit();<br />
	}<br />
	</code>
	<br />
	<br />
	<h3>Código Refatorado:</h3>
	<code>
	if ($_COOKIE['Loggedin'] == true || $_SESSION['loggedin'] == true) {<br />
	&nbsp;&nbsp;header("Location: http://www.google.com");<br />
	&nbsp;&nbsp;exit();<br />
	}<br />
	</code>
</body>
</html>