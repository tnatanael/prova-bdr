<?php
/*
*	3. Refatore o código abaixo, fazendo as alterações que julgar necessário.
*/
?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">

    <title>Prova BDR - Questão 3</title>
    <meta name="description" content="Prova-BDR">
    <meta name="author" content="Thiago Natanael">
</head>
<body>
	<a href=javascript:history.back(2)>Voltar</a>
	<br />
	<h3>Observações</h3>
       	Esta API responderá aos verbos PUT, POST, GET, DELETE<br />
       	Utilizei apenas uma biblioteca de abstração para facilitar a interação com o BD.<br />
       	Posso apresentar uma versão usando apenas as funções nativas do PHP, ou cakePHP também se preferirem.<br />
       	Usei uma abordagem de implementação parcial de MVC e OOP, espero que gostem.<br />

		Todas as informações sobre documentação da API podem ser encontradas no README.md do repositório.
</body>
</html>