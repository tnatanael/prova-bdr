<!doctype html>

<html lang="pt-BR">
<head>
    <meta charset="utf-8">

    <title>Prova BDR - Interface para a API</title>
    <meta name="description" content="Prova-BDR">
    <meta name="author" content="Thiago Natanael">

  <link rel="stylesheet" href="css/bootstrap.min.css?v=1.0">
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand">Tarefas</a>
	    </div>
	    <div class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	        <li class="active"><a class="btn" id="menu_lista">Lista</a></li>
	        <li><a class="btn" id="menu_cadastrar">Cadastrar</a></li>
	      </ul>
	    </div><!--/.nav-collapse -->
	  </div>
	</div>
	<br />
	<br />
	<div id="cadastrar" class="container-fluid hide">
	  <div class="text-center">
	    <h1>Cadastrar Tarefa</h1>
	  </div>
	  <form class="form-horizontal" role="form">
	  	<input type="hidden" id="id">
	    <div class="row">
	      <div class="col-sm-6 col-lg-4">
	        <div class="form-group">
	          <label for="inputPassword" class="col-md-4 control-label">Título:</label>
	          <div class="col-md-8">
	            <input type="text" class="form-control" id="titulo" placeholder="Título da Tarefa">
	          </div>
	        </div>
	      </div>
	      <div class="col-sm-6 col-lg-4">
	        <div class="form-group">
	          <label for="inputLabel3" class="col-md-4 control-label">Descrição:</label>
	          <div class="col-md-8">
	            <input type="text" class="form-control" id="descricao" placeholder="Descrição da Tarefa">
	          </div>
	        </div>
	      </div>
	      <div class="col-sm-6 col-lg-4">
	        <div class="form-group">
	          <label for="inputLabel4" class="col-md-4 control-label">Ordem:</label>
	          <div class="col-md-8">
	            <input type="text" class="form-control" id="ordem" placeholder="Ordem">
	          </div>
	        </div>
	      </div>	      
	    </div><!-- /.row this actually does not appear to be needed with the form-horizontal -->
	  </form>
  		<div class="text-center">
	    	<button id="btn_cadastrar" type="button" class="btn btn-primary">Cadastrar</button>
	  	</div>
	</div><!-- /.container -->

	<div id="lista" class="container-fluid">
	  	<div class="text-center">
	    	<h1>Tarefas</h1>
	  	</div>
		<div id="lista_tarefas"></div>
	</div>
	
	<script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="interface.js"></script>
</body>
</html>