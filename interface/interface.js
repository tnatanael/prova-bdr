// A $( document ).ready() block.
$( document ).ready(function() {
	//EVENTOS
    $('#menu_cadastrar').on('click', function(){
    	$('.active').removeClass('active');
    	$(this).parent().addClass('active');

    	$('#cadastrar').removeClass('hide');
    	$('#lista').addClass('hide');
    	
    });
    $('#menu_lista').on('click', function(){
    	$('.active').removeClass('active');
    	$(this).parent().addClass('active');

    	$('#cadastrar').addClass('hide');
    	$('#lista').removeClass('hide');

    	busca_tarefas();
    });

    $('#btn_cadastrar').on('click', function(){
    	$('.active').removeClass('active');
    	$(this).parent().addClass('active');

    	$('#cadastrar').addClass('hide');
    	$('#lista').removeClass('hide');
    });

    function busca_tarefas(){
    	$.ajax({
			dataType: "json",
			url: '../../api/api.php/tarefas',
			type: "GET",
		  	success: function(json) {
		  		var content = '';

             	for(var key in json) {
             		content+= '<div class="well">';
             		content+= 'ID: '+json[key].id+'  ';
             		content+= 'Titulo: '+json[key].titulo+'  ';
             		content+= 'Descricao: '+json[key].descricao+'  ';
             		content+= 'Ordem: '+json[key].ordem+'  ';
             		content+= '</div>';
             	}

             	$('#lista_tarefas').html(content);
            }
		});
   	}

    function cria_tarefas(){
    	/*$.ajax({
			dataType: "json",
			url: url,
			type: "POST",
			data: data,
		  	success: success
		  	headers: { 
		      "Content-Type": "application/json",
		      "X-HTTP-Method-Override": "PUT" },
		    }
		});*/
   	}

   	busca_tarefas();
});