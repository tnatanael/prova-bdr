<!doctype html>

<html lang="pt-BR">
<head>
    <meta charset="utf-8">

    <title>Prova BDR</title>
    <meta name="description" content="Prova-BDR">
    <meta name="author" content="Thiago Natanael">
</head>

<body>
	<h3>Prova BDR - Questões</h3>
	<a href="questoes/questao1.php">Questão 1</a>
	<br />
	<a href="questoes/questao2.php">Questão 2</a>
	<br />
	<a href="questoes/questao3.php">Questão 3</a>
    <br />
    <br />
    A Questão 4 está dividida em 2 Etapas
    <br />
    <a href="api/api.php">Questão 4 - API Rest</a>
    <br />
    <a href="questao4_interface.php">Questão 4 - Interface</a>
    <br />
</body>
</html>