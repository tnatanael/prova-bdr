CREATE SCHEMA `provabdr` ;

CREATE TABLE `provabdr`.`tarefas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(255) NULL,
  `ordem` INT NOT NULL,
  PRIMARY KEY (`id`));