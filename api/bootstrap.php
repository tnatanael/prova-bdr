<?php

//CONFIGURAÇÃO DO DATABASE

//Include da Biblioteca de Abstração MySQL
require_once 'libs/meekrodb.2.3.class.php';

//Buscamos as informações de conexão do DB
$dbinfo = json_decode(file_get_contents('dbconf.json'));

//Fornecemos informações de conxão para a biblioteca
DB::$user = $dbinfo->user;
DB::$password = $dbinfo->pass;
DB::$dbName = $dbinfo->db;
DB::$host = $dbinfo->host;
DB::$port = $dbinfo->port;

//Carrega as rotas
require_once 'router.php';