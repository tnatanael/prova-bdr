<?php
/*
*    Questão 4 - Parte 1 API Rest
*
*    4. Desenvolva uma API Rest para um sistema gerenciador de tarefas
*       (inclusão/alteração/exclusão). As tarefas consistem em título e descrição, ordenadas por
*       prioridade.
*       Desenvolver utilizando:
*       • Linguagem PHP (ou framework CakePHP);
*       • Banco de dados MySQL;
*
*/

//Bootstrapper criar a base para a aplicação
require_once 'bootstrap.php';

//Cria o ambiente padrão para uma API Rest

//Tratamos os parâmetros
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

//Validamos se o primeiro parâmetro do request é para uma rota existente
if(in_array($request[0], $routes))
{
    //Se a rota existe carregamos seu controller e model
    require_once 'controllers/'.$request[0].'Controller.php';

    $controller_class = ucfirst($request[0]).'Controller';

    switch ($method) {
        case 'PUT':
            //Corrige o acesso aos dados do PUT pelo PHP
            parse_str(file_get_contents("php://input"),$_POST);
            $response = $controller_class::put();
            break;
        case 'POST':
            $response = $controller_class::post();
            break;
        case 'GET':
            //Traduzimos o GET para get ou show caso haja um segundo parâmetro na rota
            if($request[1]){
                $response = $controller_class::show($request[1]);
            }else{
                $response = $controller_class::get();
            }
            break;
        case 'DELETE':
            $response = $controller_class::delete($request[1]);
            break;
        default:
            $response = array(
                "error" => "Verbo não suportado"
            );
            break;
    }
}else{
    $response = array(
        "error" => "Recurso não encontrado"
    );
}

//Por fim printamos o resultado da requisição no formado JSON
exit(json_encode($response));

?>