<?php

class TarefasController {

	//Buscar Tarefa
	public static function get()
	{
	  	return DB::query("SELECT * FROM tarefas");
	}
	
	public static function show($id)
	{
	  	return DB::query("SELECT * FROM tarefas WHERE id = ".$id);
	}

	public static function put()
	{
		//Verifica os parâmetros e monta a query
		$query = "UPDATE tarefas 
					 SET ";
		if($_POST['titulo']){
			$query.= "titulo = '".$_POST['titulo']."', ";
		}
		if($_POST['descricao']){
			$query.= "descricao = '".$_POST['descricao']."', ";
		}
		if($_POST['ordem']){
			$query.= "ordem = ".$_POST['ordem']." ";
		}				
		$query.= "WHERE id = ".$_POST['id'];
					     
		if(DB::query($query))
		{
			return array(
				'success' => 'true'
			);
		}else{
			return array(
				'error' => 'Erro ao atualizar tarefa!'
			);
		}
	}

	public static function post()
	{
		//Verifica os parâmetros e monta a query
		$query = "INSERT INTO tarefas 
					(titulo, descricao, ordem)
				  VALUES (";
		$query.= ($_POST['titulo']) ? "'".$_POST['titulo']."', ": "'', ";
		$query.= ($_POST['descricao']) ? "'".$_POST['descricao']."', ": "'', ";
		$query.= ($_POST['ordem']) ? $_POST['ordem']." ": "''";
		$query.= ")";
					     
		if(DB::query($query))
		{
			return array(
				'success' => 'true'
			);
		}else{
			return array(
				'error' => 'Erro ao inserir tarefa!'
			);
		}
	}

	public static function delete($id)
	{
		$query = "DELETE FROM tarefas WHERE id = ".$id;
		if(DB::query($query))
		{
			return array(
				'success' => 'true'
			);
		}else{
			return array(
				'error' => 'Erro ao excluir tarefa!'
			);
		}
	}
}