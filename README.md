## Sinopse

Repositório contendo conteúdo da prova solicitada pela Aline Neves da BDR, na data de 30/09/2015 as 14:25., tendo como objetivo a conclusão das tarefas descritas no PDF prova.pdf que pode ser encontrado na raiz deste repositório.

## Requisitos

PHP 5.3+
MySQL

## Instalação

Para executar o projeto é necessário um servidor web com PHP 5.3 ou superior instalado e apontando para o arquivo index.php que fica na raiz do repositório!

Ao executar o projeto será exibido uma página contendo um indice com os links para as respostas das questões.

**API**

A api solicitada na questão 4 encontra-se dentro da pasta **api** do repositório.

Para que funcione é necessário configurar o arquivo dbconf.json, que fica dentro da pasta **api**, com informações de acesso a um database para o projeto.

Os comandos SQL para criar o database encontram-se também na pasta **api** no arquivo database.sql.

Após a configuração do database, a api deverá responder no formato JSON as seguintes rotas:
   
    Verbo: GET 
    Rota: '../api/api.php/tarefas'
    Descrição: Retornará todas as tarefas


    Verbo: GET 
    Rota: '../api/api.php/tarefas/{id}'
    Descrição: Retornará apenas a tarefa especificada pelo id


    Verbo: POST
    Parâmetros: titulo, descricao, ordem
    Rota: '../api/api.php/tarefas'
    Descrição: Para cadastrar uma tarefa


    Verbo: PUT
    Parâmetros: id, titulo, descricao, ordem
    Rota: '../api/api.php/tarefas'
    Descrição: Para alterar uma tarefa


    Verbo: DELETE 
    Rota: '../api/api.php/tarefas/{id}'
    Descrição: Para excluir uma tarefa
    

Para os verbos PUT, POST e DELETE o retorno deverá conter, no formato JSON, apenas success = true.

**Interface Simples Usando Javascript**

A interface solicitada na questão 4 se encontra na pasta **interface**, infelizmente não consegui concluír devido ao horário.

Realizei a implementação das telas usando Bootstrap, obedecendo os padrões de responsividade.
A página de listagem de tarefas, está funcionando! Ela executa uma chamada na API usando AJAX e traz as tarefas cadastradas.

Gostaria de ter terminado, mas já são 3 da manhã e o cansado bate.


Obrigado!

## Contribuidores

Thiago Natanael

## Licensa

Este repositório é de livre acesso, e seu conteúdo está disponível para todos.
